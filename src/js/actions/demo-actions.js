import { ADD_ARTICLE } from "../constants/action-types";
import axios from 'axios';

//demo data
export const getData = () => {
    return dispatch => {
        axios.get(`https://jsonplaceholder.typicode.com/posts`).then(response => {
            if (response) {
                console.log("respomnse", response)
                if (response.data) {
                    dispatch({ type: ADD_ARTICLE, payload: response });
                }
            }
        })
    }
}
