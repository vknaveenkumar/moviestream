import { ADD_ARTICLE } from "../constants/action-types";

const initialState = {
  articles: []
};



export default (state = initialState, action) => {

  switch (action.type) {
    case ADD_ARTICLE:
      return {
        ...state, articles: [...state.articles, action.payload.data]
      };
    default:
      return state;
  }

};


