import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { debounce } from 'throttle-debounce';
import Search from "../components/Search/Search";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

import Container from '@material-ui/core/Container';
import Navbar from '../components/Navbar/Navbar'
import Layout from '../components/Layout/Layout'
import { BASE_URL } from '../../config/env'

//demo actions
import * as actions from '../actions/demo-actions';

/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */


const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    cardBody: {
        height: '100px',
        overflow: 'hidden'
    }

});

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            movieList: [],
        }
    }




    /**
     * 
     * @param {*} type 
     * @param {*} search 
     * Load Movies Based on filter
     */
    filter = (type, search) => {
        let params = ''
        if (type === 'genres') {
            if (search !== '') {
                params = `genre=${search}`;
            }
        } else {
            if (search !== '') {
                params = `title=${search}`
            }
        }


        axios.get(`${BASE_URL}/movies?${params}`).then(res => {
            console.log('tes', res)
            if (res.length >= 0 || res.data !== undefined && res.data.length > 0) {
                this.setState({
                    movieList: res.data
                })
            } else {

            }

        }).catch(err => {

        })
    }

    watchMovies = (movieId) => {
        console.log('-----',movieId)
       // console.log('-----',JSON.parse(localStorage.getItem('user')).id)
        axios.post(`${BASE_URL}/user-movies`, {
            userId: JSON.parse(localStorage.getItem('user')).userId,
            movieId: movieId
        }).then(res => {
             console.log(res)
        })
    }


    render() {
        const { classes } = this.props;
        const { movieList } = this.state


        return (
            <React.Fragment>
                <Navbar
                    appBarColor={'#1976d2'}
                    {...this.props}
                />



                <div style={{ backgroundColor: movieList.length > 0 ? '#f5f5f5' : 'none', paddingTop: '2%' }}>
                    <Search filter={debounce(1000, this.filter)} />
                    <Container >
                        {
                            movieList.length > 0 ? <Layout classes={{
                                cardBody: classes.cardBody,
                            }} items={movieList} direction={"row"} dashboard={true} watchMovies={this.watchMovies} /> :
                                <Typography component="h1" variant="h5">
                                    Currently Movies are not available
                        </Typography>
                        }
                    </Container >

                </div>
            </React.Fragment>
        );
    }
}

// const mapStateToProps = state => {
//     return { articles: state.articles };
// };

// function mapDispatchToProps(dispatch) {
//     return {
//         actions: bindActionCreators(actions, dispatch)
//     };
// }
// export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Login))

export default withStyles(styles, { withTheme: true })(Login);
