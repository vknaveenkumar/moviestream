import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { BASE_URL } from '../../config/env'
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import SaveIcon from '@material-ui/icons/Save';
import Container from '@material-ui/core/Container';
import Navbar from '../components/Navbar/Navbar'
import Layout from '../components/Layout/Layout'
import Popup from '../components/Popup/Popup'


/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */


const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    cardBody :{
        height: '100px',
        overflow: 'hidden'
    }


});

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            products: [],
            cartItems: { id: 0, userId: 0, cart: [] },
            searchFilter: '',
            orderPopup: false
        }
    }

    /**
     * This will get the cart times
     */

    componentDidMount() {
        if (localStorage.getItem('petKartUserInfo') !== null && localStorage.getItem('petKartUserInfo') !== undefined) {
            const userId = JSON.parse(localStorage.getItem('petKartUserInfo')).id;
            axios.get(`${BASE_URL}/carts?userId=${userId}`).then(res => {
                // axios.get(`http://localhost:3001/carts/userId=${userId}`).then(res => {
                //console.log("ssssss", res.data[0])
                this.setState({
                    cartItems: res.data[0]
                })
            })
        }
    }



    /**
     * 
     * @param {*} item 
     * Remove the item from the cart
     */
    removeFromCart = (item) => {
        const { cartItems } = this.state

        let removedCart = cartItems.cart.filter(data => data.id !== item.id)
        console.log(JSON.parse(localStorage.getItem('petKartUserInfo')))
        let cartItem = {
            id: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
            cart: removedCart,
            userId: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
        }



        axios.put(`${BASE_URL}/carts/${cartItem.id}`, {
            id: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
            cart: removedCart,
            userId: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
        }).then(() => {
            this.setState({
                cartItems: cartItem
            })
        })
    }

    /**
     * 
     * @param {*} event 
     * on Change Filter
     */
    handleSearchFilter = (event) => {
        this.setState({
            searchFilter: event.target.value
        })
    }
    /**
     * 
     * @param {*} products 
     * This method filter the products based on search input
     */
    renderFilteredProduct = (products) => {
        const { searchFilter } = this.state;
        if (searchFilter === '' || searchFilter === null) {
            return products
        }
        let filteredText = products.filter(message => message.name.toUpperCase().indexOf(searchFilter.toUpperCase()) >= 0)
        return filteredText
    }

    /**
     * Order the items and will clear from the cart
     */
    orderItems = () => {
        axios.post(`${BASE_URL}/orders`, {
            userId: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
            items: this.state.cartItems.cart
        }).then(res => {
            axios.put(`${BASE_URL}/carts/${JSON.parse(localStorage.getItem('petKartUserInfo')).id}`, {
                id: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
                userId: JSON.parse(localStorage.getItem('petKartUserInfo')).id,
                cart: []
            }).then(() => {
                const { cartItems } = this.state
                cartItems.cart = []
                this.setState({
                    cartItems
                })
            })
        })
    }

    /**
     * Close and open modal popup
     */
    toggleOrderPopup = () => {
        this.setState(
            prevState => {
                return {
                    orderPopup: !prevState.orderPopup
                };
            }
        );
    };

    /**
     * close modal popup and call orders items method
     */

    confirmOrder = () => {
        this.toggleOrderPopup()
        this.orderItems()
    }


    renderCardFooter = (item) => {
        return (
            <Button size="small" color="secondary" onClick={() => { this.removeFromCart(item) }}>
                REMOVE FROM CART <RemoveShoppingCartIcon />
            </Button>
        )
    }


    render() {
        const { classes } = this.props;
        const { products, cartItems, orderPopup } = this.state

        let filteredProducts = this.renderFilteredProduct(cartItems.cart)
        return (
            <React.Fragment>
                <Navbar {...this.props} cartItems={cartItems.cart} noOfItemsInCart={cartItems.cart.length} searchFilter={this.handleSearchFilter} removeFromCart={this.removeFromCart} />
                <div style={{ marginTop: "25px" }}></div>
                {
                    cartItems.cart !== undefined && cartItems.cart !== null && <Container >
                        <Layout classes={classes} items={filteredProducts} direction={"row"} renderCardFooter={this.renderCardFooter} cartItems={cartItems.cart} dashboard={false} removeFromCart={this.removeFromCart} />
                    </Container >
                }
                <div style={{ margin: "25px" }}>{
                    cartItems.cart.length > 0 ? <Grid container justify="center" >
                        <Grid item>
                            <Button
                                variant="contained"
                                color="primary"
                                size="small"
                                className={classes.button}
                                startIcon={<SaveIcon />}
                                onClick={this.toggleOrderPopup}
                            >
                                PLACE ORDER
                        </Button>
                        </Grid>
                    </Grid> : <Typography gutterBottom variant="h5" component="h2">
                            No items in the cart
                        </Typography>
                }

                </div>

                <Popup isOpen={orderPopup}
                    modalTitle={'Order Confirmation'}
                    modalDescription={'Going to Place Order.Please confirm'}
                    modalFooter={
                        <Button
                            onClick={this.confirmOrder}
                            variant="contained"
                            color="primary"
                        >
                            PLACE ORDER
                         </Button>
                    }
                    handleClose={this.toggleOrderPopup}
                />
            </React.Fragment>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Login);
