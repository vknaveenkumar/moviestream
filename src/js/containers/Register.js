import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import MovieStream from '../components/Logo/Logo'
import axios from 'axios';
import Background from '../../images/BG_1.jpg'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { withStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { BASE_URL } from '../../config/env'




const styles = theme => ({
    root: {
        [theme.breakpoints.up('lg')]: {
            position: 'absolute',
            right: 0,
            top: '5%'

        },
    },
    radio: {
        '&$checked': {
            color: '#4B8DF8'
        }
    },
    checked: {

    },
    paper: {
        // marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        // [theme.breakpoints.up('lg')]: {
        backgroundColor: 'white',
        padding: '20px',
        borderRadius: '15px'
        //},
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },

});

/**
 * Register COntainer
 */

export class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            email: '',
            cpassword: '',
            userType: 'trial',
            errorText: '',
            onSubmit: false,
            successText : false,
        }
    }

    /**
     * 
     * @param {*} e 
     * Changing state for input field
     */
    _handleTextFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    /**
     * Handler method for register
     */
    _handleRegister = () => {
        const { username, email, userType, password, fname, lname, cpassword } = this.state

        this.setState({
            onSubmit: true
        })

      
        if(!this.validateEmail(email)){
            this.setState({
                successText: '',
                errorText: 'Enter Proper Mail ID'
            })
            return ;
        }

        if(!this.validatePassword(password)){
            this.setState({
                successText: '',
                errorText: 'Password must be at least 6 characters in length.,Password must contain at least 1 uppercase characters.,Password must contain at least 1 digit characters.,Password must contain at least 1 special characters.'
            })
            return ;
        }

        // 'trail' or 'paid'
        if (username !== '' && password !== '' && fname !== '' && lname !== '') {
            if (password === cpassword) {
                axios.post(`${BASE_URL}/users`, {
                    userName: username,
                    email: email,
                    password: password,
                    userType: userType,
                }).then(() => {
                    this.setState({
                        username: '',
                        password: '',
                        email:'',
                        cpassword: '',
                        userType: 'trial',
                        errorText: '',
                        successText: 'Success',
                        onSubmit : true
                    })
                }).catch(err => {
                    console.log(err.response)
                    if (err.response.data.errorCode > 4000) {
                        this.setState({
                            successText: '',
                            errorText: err.response.data.errorMessage.toString()
                        })
                    }
                })


            } else {
                this.setState({
                    errorText: 'Password and Confirm Password are not same'
                })
            }

        }
    }

    /**
     * @param {*} email
     * Checking the valid mail 
     */
    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    /**
     * validatePassword
     */
    validatePassword = (password) => {
        const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        return re.test(String(password).toLowerCase());
    }

    render() {
        const { classes } = this.props;
        const { email, username, userType, password, cpassword, errorText, onSubmit,successText } = this.state
        console.log(this.state)
     return (
            <div style={{ backgroundImage: 'url(' + Background + ')', height: '100vh' }}>
                <MovieStream />
                <Container className={classes.root} component="main" maxWidth="xs">
                    <CssBaseline />
                    <div className={classes.paper}>

                        <Typography component="h1" variant="h5">
                            Register your account
                    </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                value={email}
                                label="Email"
                                name="email"
                                autoComplete="username"
                               // value={this.state.textFieldValue}
                                onChange={this._handleTextFieldChange}
                                autoFocus
                                error={onSubmit && email.length < 0}
                            />

                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                value={username}
                                fullWidth
                                id="email"
                                label="User Name"
                                name="username"
                                autoComplete="username"
                               // value={this.state.textFieldValue}
                                onChange={this._handleTextFieldChange}
                                autoFocus
                                error={onSubmit && username.length < 0}
                            />

                            <FormLabel style={{ marginTop: '2%' }} component="legend">User Type</FormLabel>

                            <RadioGroup aria-label="userType" style={{ flexDirection: 'row' }} name="userType" value={userType} onChange={this._handleTextFieldChange}>
                                <FormControlLabel value="trial" control={<Radio classes={{ root: classes.radio, checked: classes.checked }} />} label="Trial" />
                                <FormControlLabel value="paid" control={<Radio classes={{ root: classes.radio, checked: classes.checked }} />} label="Paid" />
                            </RadioGroup>


                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                value={password}
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                               // value={this.state.textFieldValue}
                                onChange={this._handleTextFieldChange}
                                autoComplete="current-password"
                                error={onSubmit && password.length < 0}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                value={cpassword}
                                name="cpassword"
                                label="Confirm Password"
                                type="password"
                                id="password"
                                //value={this.state.textFieldValue}
                                onChange={this._handleTextFieldChange}
                                autoComplete="current-password"
                                error={onSubmit && cpassword.length < 0}
                            />
                            {errorText.length > 0 &&
                                <Typography variant="body2" color="error" component="p">
                                    {errorText}
                                </Typography>}
                            {successText.length > 0 &&
                                <Grid container>
                                    <Grid item>
                                        <Link href="/" variant="body2">
                                            {"Register Succcessfull . Click here to Login Page"}
                                        </Link>
                                    </Grid>
                                </Grid>
                            }

                            <Button

                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={this._handleRegister}
                                className={classes.submit}
                            >
                                Register
                        </Button>
                            <Grid container>
                                <Grid item>
                                    <Link href="/" variant="body2">
                                        {"Have an account? Login"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                    {/* <Box mt={8}>
                        <Copyright />
                    </Box> */}
                </Container>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Register);
