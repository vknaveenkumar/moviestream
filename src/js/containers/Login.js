import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Background from '../../images/BG_1.jpg'
import Link from '@material-ui/core/Link';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { withStyles } from "@material-ui/core/styles";
import MovieStream from '../components/Logo/Logo'
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { BASE_URL } from '../../config/env'
import { dark } from '@material-ui/core/styles/createPalette';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                MovieStream
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}



const styles = theme => ({
    root: {
        [theme.breakpoints.up('lg')]: {
            position: 'absolute',
            right: 0,
            marginTop: '10%',
        },
    },
    paper: {
        //marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        // [theme.breakpoints.up('lg')]: {
        backgroundColor: 'white',
        padding: '20px',
        borderRadius: '30px'
        //},

    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },

});

/**
 * Login Contaier
 */

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            onSubmit: false,
            loginError: false,
            errorText : ''
        }
    }

    /**
     * @param {*} e 
     * Changing state for input field
     */
    _handleTextFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    /**
     * @param {*} 
     *  Check the Login
     */

    _handleLogin = () => {
        const { email, password } = this.state
        this.setState({
            onSubmit: true
        })
        axios.post(`${BASE_URL}/login`, {
            email: email,
            password: password,
        }).then((data) => {
            console.log(data.data)
            localStorage.setItem('user',JSON.stringify(data.data));
            this.props.history.push('/dashboard')
        }).catch(err => {
          // console.log(err.response)
            if (err.response.status >= 400) {
                console.log(err.response.data)
                this.setState({
                    loginError : true,
                    errorText: err.response.data.message
                })
            }
        })
    }

    render() {
        const { classes } = this.props;
        const { onSubmit, email, password, loginError,errorText } = this.state
//console.log('errorText',loginError,errorText)
        //style={{backgroundImage:'url(https://i.stack.imgur.com/N3lSw.png)'}}
        //  style={{backgroundImage:'url('+Background+')'}}
        return (
            <div style={{ backgroundImage: 'url(' + Background + ')', height: '100vh' }}>
                 <MovieStream/>
                <Container className={classes.root} maxWidth="xs">
                    {/* <CssBaseline /> */}
                   
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>

                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                    </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email"
                                name="email"
                                autoComplete="username"
                                value={email}
                                onChange={this._handleTextFieldChange}
                                autoFocus
                                error={onSubmit && email.length <= 0}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                value={this.state.password}
                                onChange={this._handleTextFieldChange}
                                autoComplete="current-password"
                                error={onSubmit && password.length <= 0}
                            />

                            {loginError &&
                                <Typography variant="body2" color="error" component="p">
                                   {errorText}
                        </Typography>}

                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={this._handleLogin}
                                className={classes.submit}
                            >
                                Sign In
                        </Button>
                            
                            <Grid container>
                                <Grid item>
                                    <Link href="/register" variant="body2">
                                        {"Don't have an account? Sign Up"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                    <Box mt={8}>
                        <Copyright />
                    </Box>
                </Container>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Login);
