import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import { withStyles } from "@material-ui/core/styles";
import CardContent from './CardContent'
import Snackbar from '@material-ui/core/Snackbar';



const styles = () => ({
  cardWidth: {
    minWidth: 300,
  },
  media: {
    height: 140,
  },
  cardBody: {

  }
});



function MediaCard(props) {

  const { classes,item } = props

  const [status,setStatus] = useState(false)

  const toggleSnackBar = (movieId) =>{

      props.watchMovies(movieId)
      setStatus(!status)
  }

  return (
    <Card className={classes.cardWidth}>
      <CardActionArea>
        <CardContent item={item} toggleSnackBar={toggleSnackBar}/>

      </CardActionArea>
      {/* <CardActions>
        {renderCardFooter(props.item)}
      </CardActions> */}

      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={status}
        onClose={toggleSnackBar}
        autoHideDuration={3000}
        message="Playing Videos"
      />
    </Card>
  );
}


export default withStyles(styles)(MediaCard);
