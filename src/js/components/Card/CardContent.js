import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from "@material-ui/core/styles";
import Joker from '../../../images/2.jpg'
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';


const styles = theme => ({
    root: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        justify: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
});


function MovieStreamCard({ classes, toggleSnackBar, item: { title, genre, movieId } }) {
    //  const classes = useStyles();
    //const theme = useTheme();
   

    return (
        <Card className={classes.root}>
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h6">
                        {title}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {genre}
                    </Typography>
                </CardContent>
                <div className={classes.controls}>
                    <IconButton aria-label="previous">
                        {/* {theme.direction === 'rtl' ? <SkipNextIcon /> : <SkipPreviousIcon />} */}
                    </IconButton>
                    <IconButton aria-label="play/pause">
                        <PlayArrowIcon className={classes.playIcon} onClick={() => { toggleSnackBar(movieId) }} />
                    </IconButton>
                    <IconButton aria-label="next">
                        {/* {theme.direction === 'rtl' ? <SkipPreviousIcon /> : <SkipNextIcon />} */}
                    </IconButton>
                </div>
            </div>
            <CardMedia
                className={classes.cover}
                style={{marginLeft:'auto'}}
                image={Joker}
                title="Live from space album cover"
            />
        </Card>
    );
}

export default withStyles(styles)(MovieStreamCard);